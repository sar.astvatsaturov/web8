const getItemsFromLocalStorage = () => {
  if (localStorage.getItem('username') !== null)
    $('#username').val(localStorage.getItem('username'));
  if (localStorage.getItem('email') !== null)
    $('#email').val(localStorage.getItem('email'));
  if (localStorage.getItem('message') !== null)
    $('#message').val(localStorage.getItem('message'));
  if (localStorage.getItem('checkbox') !== null) {
    $('#checkbox').prop('checked', localStorage.getItem('checkbox') === 'true');
    if ($('#checkbox').prop('checked')) $('#Send').removeAttr('disabled');
  }
};

const setItemsToLocalStorage = () => {
  localStorage.setItem('username', $('#username').val());
  localStorage.setItem('email', $('#email').val());
  localStorage.setItem('message', $('#message').val());
  localStorage.setItem('checkbox', $('#checkbox').prop('checked'));
};

const clearLocalStorage = () => {
  localStorage.clear();
  $('#username').val('');
  $('#email').val('');
  $('#message').val('');
  $('#checkbox').prop('checked', false);
};

$(document).ready(function () {
  getItemsFromLocalStorage();
  $('.Press').click(() => {
    $('.hiddenDivision').css('display', 'flex');
    history.pushState(true, '', './form');
    clearLocalStorage();
  });
  $('.Close').click(function () {
    $('.hiddenDivision').css('display', 'none');
    history.pushState(false, '', '.');
    clearLocalStorage();
    $('#Send').attr('disabled', '');
  });
  $('#form').submit(function (e) {
    e.preventDefault();
    $('.hiddenDivision').css('display', 'none');
    $('#Send').attr('disabled', '');
    let data = $(this).serialize();
    let username;
    if ($('#username').val() !== '') username = $('#username').val();
    else username = 'user';
    let email;
    if ($('#email').val() !== '') email = $('#email').val();
    else email = 'none';
    let message;
    if ($('#message').val() !== '') message = $('#message').val();
    else message = 'none';
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: 'https://formcarry.com/s/NA8SFRVtqW',
      data: data,
      success: function (error) {
        setTimeout(() => {
          $('.Response').css('display', 'flex');
        }, 250);
        if (error.status === 'success') {
          $('.Username').text('Username: ' + username);
          $('.Email').text('Email: ' + email);
          $('.Message').text('Message: ' + message);
        } else alert('Error: ' + error.status);
      },
    });
  });
  $('.close').click(function () {
    console.log('Loading......')
    $('.Response').css('display', 'none');
    history.pushState(false, '', '.');
  });
  $('#checkbox').change(function () {
    if (this.checked) $('#Send').removeAttr('disabled');
    else $('#Send').attr('disabled', '');
  });
  $('#form').change(setItemsToLocalStorage);

  window.onpopstate = function (e) {
    if (e.state) $('.hiddenDivision').css('display', 'flex');
    else {
      $('.hiddenDivision').css('display', 'none');
    }
  };
});